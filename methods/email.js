// Mailgun mail content
const mailTestLink = 'https://www.w3schools.com/test/names.asp';
const fromMail = 'dev@predator-digital.com';

const sendVerificationMail = (userMail, storeName ,verification_code,rootUrl) => {
	const tempUrl = `${rootUrl || mailTestLink}/signupverification`;
	return {
		from: `Excited User <${fromMail}>`,
		to: `${userMail}`,
		subject: 'Verification mail',
		html: `Please use the below link to verify your email address
		<p>${tempUrl}?email=${userMail}&verification_code=${verification_code}&storeName=${storeName}</p>
		`
	};
};

const forgotPasswordMail = (userMail,rootUrl,temp) => {
	return {
		from: `Store owner <${fromMail}>`,
		to: `${userMail}`,
		subject: 'Forgot password',
		html: `<p>Please use this link to reset your password
		${rootUrl}/resetpassword?email=${userMail}&code=${temp}</p>`
	}
};

module.exports = {
	sendVerificationMail,
	forgotPasswordMail
}